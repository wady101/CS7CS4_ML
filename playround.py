import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from sklearn import datasets, linear_model
from sklearn.metrics import mean_squared_error, r2_score
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LinearRegression
from mpl_toolkits.mplot3d import Axes3D
from sklearn.preprocessing import LabelEncoder
from sklearn import preprocessing

def changetype(df):
  dfList = df.tolist()
  returnList = [ float(x) for x in dfList ]
  return returnList

def main():

  # train_data_list=list()
  # with open('./dataset/training.csv', 'r') as f:
  #   reader = csv.reader(f)
  #   next(reader)
  #   for row in reader:
  #     train_data_list.append(row)
  #   train_data= np.asarray(train_data_list)

  # print ("Train_data_list shape: ", train_data_list[1,1])
  # print ("Train_data shape: ", train_data.shape)
  # X=train_data_list[:, [10,11]]
  # y=train_data_list[:, [0]]
  # print ("X_shape",X.shape)
  #x_min, x_max = X[:, 0].min() - .5, X[:, 0].max() + .5
  #y_min, y_max = X[:, 1].min() - .5, X[:, 1].max() + .5
  # Above code is related to setting up the data as numpy and lists

  train_df=pd.read_csv('./dataset/training.csv')
  # test_data=pd.read_csv('./dataset/test.csv')
  train_data=pd.DataFrame(train_df,columns=['Year of Record','Age','Income in EUR','Gender','Country','Profession','University Degree', 'Size of City', 'Wears Glasses', 'Hair Color', 'Body Height [cm]'])
  train_data=train_data.dropna()
  print(train_data.dtypes)


   # Pre-processing of data
  le = LabelEncoder()
  le.fit(train_data['Gender'].astype(str))
  train_data['Gender']=le.transform(train_data['Gender'].astype(str))
  le.fit(train_data['Country'].astype(str))
  train_data['Country']=le.transform(train_data['Country'].astype(str))
  le.fit(train_data['Profession'].astype(str))
  train_data['Profession']=le.transform(train_data['Profession'].astype(str))
  le.fit(train_data['University Degree'].astype(str))
  train_data['University Degree']=le.transform(train_data['University Degree'].astype(str))
  le.fit(train_data['Size of City'].astype(str))
  train_data['Size of City']=le.transform(train_data['Size of City'].astype(str))
  le.fit(train_data['Wears Glasses'].astype(str))
  train_data['Wears Glasses']=le.transform(train_data['Wears Glasses'].astype(str))
  le.fit(train_data['Hair Color'].astype(str))
  train_data['Hair Color']=le.transform(train_data['Hair Color'].astype(str))

  Y = train_data.as_matrix(['Income in EUR'])
  X = train_data.as_matrix(['Year of Record', 'Age','Gender','Country','Profession','University Degree', 'Size of City', 'Wears Glasses', 'Hair Color', 'Body Height [cm]'])
  X = preprocessing.scale(X)
  Y = preprocessing.normalize(Y,'l1',0)
  X_train, X_test, y_train, y_test = np.asarray(train_test_split(X, Y, test_size=0.1))
  #scaler = preprocessing.RobustScaler()
  #X_train = scaler.fit_transform(X_train)
  #X_test = scaler.transform(X_test)
  ## Decreased accuracy by 50%
  #X_train = preprocessing.normalize(X_train, norm='l2')
  #X_test = preprocessing.normalize(X_test, norm='l2')


  #reg = LinearRegression(copy_X=True, fit_intercept=True, n_jobs=1, normalize=False)
  #reg.fit(X_train, y_train)
  #for alpha in [0.0001, 0.001, 0.01, 0.1, 1, 3, 10]:
  #  reg = linear_model.Lasso(alpha = alpha)
  #  reg.fit(X_train, y_train)
  for alpha in [0.0001, 0.001, 0.01, 0.1, 1, 3, 10]:
    reg = linear_model.Ridge(alpha = alpha)
    reg.fit(X_train, y_train)
  y_pred = reg.predict(X_test)
  print(X_train)
  print(y_train)
  print(X_test)
  print(y_test)
  print('Score: ', reg.score(X_test, y_test))
  print("Mean squared error: %f" % mean_squared_error(y_test, y_pred))
  print('Variance score: %.2f' % r2_score(y_test, y_pred))
  # x_line = np.arange(1980,2020).reshape(-1,1)
  # p = reg.predict(x_line).T

  # f, axarr = plt.subplots(2, sharex=True)
  # f.set_size_inches(12.5, 7.5)
  # axarr[0].plot(train_data['Year of Record'], train_data['Income in EUR'])
  # axarr[0].plot(x_line, p[0])
  # axarr[0].set_ylabel('CO2 Emissions')
  # axarr[1].plot(train_data['Year of Record'], train_data['Income in EUR'])
  # axarr[1].plot(x_line, p[1])
  # axarr[1].set_xlabel('Year')
  # axarr[1].set_ylabel('Relative temperature')
  # plt.show()

  ## Linear regression model below. Not required tbh
  # train_data_Y = train_data.iloc[:-20,[0,11]]
  # train_data_X = train_data.iloc[:-20,[0,1]]
  # test_data_Y = train_data.iloc[-20:,[0,11]]
  # test_data_X = train_data.iloc[-20:,[0,1]]

  # # test_data_X = test_data.iloc[:,2]
#   regr = linear_model.LinearRegression()

#   # Train the model using the training sets
#   regr.fit(train_data_X, train_data_Y)

#   pred_data_Y = regr.predict(test_data_X)

#   # The coefficients
#   print('Coefficients: \n', regr.coef_)
#   # The mean squared error
#   print("Mean squared error: %.2f"
#         % mean_squared_error(test_data_Y, pred_data_Y))
#   # Explained variance score: 1 is perfect prediction

#   # Plot outputs
#   plt.scatter(diabetes_X_test, diabetes_y_test,  color='black')
#   plt.plot(diabetes_X_test, diabetes_y_pred, color='blue', linewidth=3)

#   plt.xticks(())
#   plt.yticks(())

#   plt.show()
if __name__ == '__main__':
  main()
