import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from sklearn import datasets, linear_model
from sklearn.metrics import mean_squared_error, r2_score
from sklearn.model_selection import train_test_split
from sklearn.decomposition import PCA
from sklearn.ensemble import RandomForestClassifier,RandomForestRegressor
from sklearn.tree import DecisionTreeRegressor
from sklearn.datasets import make_classification
from sklearn.linear_model import LinearRegression
from mpl_toolkits.mplot3d import Axes3D
from sklearn.preprocessing import LabelEncoder, OneHotEncoder
from sklearn import preprocessing
from sklearn.pipeline import FeatureUnion, Pipeline
from sklearn.base import BaseEstimator, TransformerMixin
from sklearn.preprocessing import Imputer, StandardScaler, MinMaxScaler
from sklearn import utils
import category_encoders as ce

def changetype(df):
  dfList = df.tolist()
  returnList = [ float(x) for x in dfList ]
  return returnList



def removeOutliners(dataframe,dataframe2):
    """ Removes all the instances which has an outlier variable. """
    quantile1 = dataframe.quantile(0.25)
    quantile3 = dataframe.quantile(0.75)
    iqr = quantile3 - quantile1
    fenceLow = quantile1 - 1.5 * iqr
    fenceHigh = quantile3 + 1.5 * iqr
    fenceOut = (dataframe > fenceLow) & (dataframe < fenceHigh)
    notOutliers = fenceOut[fenceOut.columns[0]]
    for i, col in enumerate(list(fenceOut.columns)):
        if (i == 0): continue
        notOutliers = notOutliers & fenceOut[col]
    return dataframe.loc[notOutliers],dataframe2.loc[notOutliers]

def main():

  # train_data_list=list()
  # with open('./dataset/training.csv', 'r') as f:
  #   reader = csv.reader(f)
  #   next(reader)
  #   for row in reader:
  #     train_data_list.append(row)
  #   train_data= np.asarray(train_data_list)

  # print ("Train_data_list shape: ", train_data_list[1,1])
  # print ("Train_data shape: ", train_data.shape)
  # X=train_data_list[:, [10,11]]
  # y=train_data_list[:, [0]]
  # print ("X_shape",X.shape)
  #x_min, x_max = X[:, 0].min() - .5, X[:, 0].max() + .5
  #y_min, y_max = X[:, 1].min() - .5, X[:, 1].max() + .5
  # Above code is related to setting up the data as numpy and lists

  train_df=pd.read_csv('./dataset/training.csv')
  test_df=pd.read_csv('./dataset/test.csv')
  train_df = train_df.drop(["Instance"], axis=1)
  test_df = test_df.drop(["Instance"], axis=1)
  # test_data=pd.read_csv('./dataset/test.csv')
  Income_counts = train_df['Income in EUR'].value_counts()
  non_value = train_df.isnull().sum()
  nonnull = ['Year of Record','Age', 'Gender','Country','Profession','University Degree', 'Size of City', 'Wears Glasses', 'Hair Color', 'Body Height [cm]']
  for i in nonnull:
    exec ("train_df[\"%s\"].fillna(train_df[\"%s\"].value_counts().idxmax(), inplace=True)" % (i,i))
  Y = train_df["Income in EUR"]
  X = train_df.drop(['Income in EUR','Gender', 'Hair Color', 'Wears Glasses'], axis=1)
  #Y = Y.head(73230)
  #``Y = np.log(Y)
  #X = X.head(73230)

  non_value = test_df.isnull().sum()
  for i in nonnull:
    exec ("test_df[\"%s\"].fillna(test_df[\"%s\"].value_counts().idxmax(), inplace=True)" % (i,i))
  Xtst = test_df.drop(['Income','Gender', 'Hair Color', 'Wears Glasses'], axis=1)



   # Pre-processing of data
  #le = LabelEncoder()
  #oe = OneHotEncoder()
  #X = X.apply(le.fit_transform)
  # X.Gender = pd.to_numeric(X['Gender'])
  # X.Age = pd.to_numeric(X['Age'])
  # X.Country = pd.to_numeric(X['Country'])
  # X.Profession = pd.to_numeric(X['Profession'])
  # X.University = pd.to_numeric(X['University Degree'])
  # X.City = pd.to_numeric(X['Size of City'])
  # X.Glasses = pd.to_numeric(X['Wears Glasses'])
  # X.Hair  =pd.to_numeric(X['Hair Color'])
  # X.Height = pd.to_numeric(X['Body Height [cm]'])
    #le.fit(train_data['Income in EUR'].astype(str))
  #train_data['Income in EUR']=le.fit_transform(train_data['Income in EUR'].astype(str))

  #le.fit(train_data['Body Height [cm]'].astype(str))
  #train_data['Body Height [cm]']=le.transform(train_data['Body Height [cm]'].astype(str))

  ## Removes Outliners
  #train_data= removeOutliners(pd.DataFrame(train_data))

  #le = LabelEncoder()
  #Xtst = Xtst.apply(le.fit_transform)
  # Xtst.Gender = pd.to_numeric(Xtst['Gender'])
  # Xtst.Age = pd.to_numeric(Xtst['Age'])
  # Xtst.Country = pd.to_numeric(Xtst['Country'])
  # Xtst.Profession = pd.to_numeric(Xtst['Profession'])
  # Xtst.University = pd.to_numeric(Xtst['University Degree'])
  # Xtst.City = pd.to_numeric(Xtst['Size of City'])
  # Xtst.Glasses = pd.to_numeric(Xtst['Wears Glasses'])
  # Xtst.Hair  =pd.to_numeric(Xtst['Hair Color'])
  # Xtst.Height = pd.to_numeric(Xtst['Body Height [cm]'])


  SS = MinMaxScaler()
  #SS = StandardScaler()
  #pc = PCA(10)
  #ohe = ce.OneHotEncoder(handle_unknown='ignore', use_cat_names=True)
 # temp_col1 = ['Year of Record','Age', 'Country','Profession','University Degree', 'Size of City', 'Body Height [cm]']
  #ohe = ce.OneHotEncoder(handle_unknown='ignore', use_cat_names=True)
  ohe = ce.sum_coding.SumEncoder(drop_invariant=True)
  ohe.fit(X,Y)
  X = ohe.transform(X)
  Xtst = ohe.transform(Xtst)
  # ohe = preprocessing.KBinsDiscretizer(n_bins=100, encode='onehot-dense',strategy='kmeans')
  # ohe.fit(X,Y)
  # X = ohe.transform(X)
  # Xtst = ohe.transform(Xtst)

  #quantile_transformer = preprocessing.QuantileTransformer(random_state=0)
  #X = quantile_transformer.fit_transform(X)
  #Xtst = quantile_transformer.transform(Xtst)
  X = SS.fit_transform(X)
  Xtst = SS.transform(Xtst)
  print(X.shape)
  print(Xtst.shape)


  #principalComponents = pc.fit_transform(X)
  #test_data = pc.fit_transform(test_data)
    # print(pc.explained_variance_ratio_)
    # array([7.35041374e-01, 2.64935995e-01, 1.10061180e-05, 6.21704987e-06])
  #X = preprocessing.scale(X)
  #Y = preprocessing.normalize(Y,'l1',0)

  # If test_data is not avalible uncomment below line and comment the rest of them below
  # features_raw = Y
  # skewed = [test_data['Gender'], test_data['University Degree'], test_data['Hair Color']]
  # numerical = [test_data['Year of Record'], test_data['Age'], test_data['Body Height [cm]'], test_data['Size of City']]
  #tobe_normalized = X.Age, X.Country, X.Height, X.Profession

  # features_log_transformed = pd.DataFrame(data=features_raw)
  # features_log_transformed[skewed] = features_raw[skewed].apply(lambda x: np.log(x + 1))
  #scaler = MinMaxScaler()
  #np_scaled = scaler.fit_transform(tobe_normalized)
  #df_normalized = pd.DataFrame(np_scaled)# features_log_minmax_transform = pd.DataFrame(data = features_log_transformed)
  # features_log_minmax_transform[numerical] = scaler.fit_transform(features_log_transformed[numerical])
  # features_final = pd.get_dummies(features_log_minmax_transform)
  # Y = features_raw
  #scaler = preprocessing.StandardScaler()
#  scaler = preprocessing.MinMaxScaler()
  #pipeline=createPipeline(pd.DataFrame(X).columns.values)
  #X = pipeline.fit_transform(pd.DataFrame(X))
  #X = scaler.fit_transform(X)
  #pc = PCA(10)
  #X = pc.fit_transform(X)
#  Y = scaler.fit_transform(Y)
  #Y_norm = list()



  ## This is the my encoding technique here
  #for x in Y:
    # 1777867 for 30k
    # 178569.22113 for 40k and 25k
  #  if x > 20000:
  #    Y_norm.append(1)
  #  else: Y_norm.append(0)
 # Y = Y_norm

  #X = principalComponents
  X_train=X
  y_train=Y
  X_test=Xtst#.as_matrix(['Country','Profession','University Degree', 'Size of City', 'Wears Glasses', 'Hair Color', 'Body Height [cm]'])

  #X_train = scaler.fit_transform(X_train)
  #X_test = scaler.transform(X_test)

  ## Normalization Decreased accuracy by 50%
#  X_train = preprocessing.normalize(X_train, norm='l2')
#  X_test = preprocessing.normalize(X_test, norm='l2')


  #reg = LinearRegression(copy_X=True, fit_intercept=True, n_jobs=1, normalize=False)
  #reg.fit(X_train, y_train)
  #for alpha in [0.0001, 0.001, 0.01, 0.1, 1, 3, 10]:
  #  reg = linear_model.Lasso(alpha = alpha)
  #  reg.fit(X_train, y_train)
  #for alpha in [0.0001, 0.001, 0.01, 0.1, 1, 3, 10]:
     #reg = linear_model.Ridge(alpha = alpha)
  print(X.shape)
  print(utils.multiclass.type_of_target(X_train))
  print(utils.multiclass.type_of_target(y_train))
  y_train = np.ravel(y_train)
  #reg = RandomForestRegressor(n_estimators=50, random_state=10, n_jobs=4,max_depth=4)
  #reg = RandomForestRegressor(min_samples_split=4000, n_estimators=50, min_samples_leaf=1000, n_jobs=-1)
  reg = RandomForestRegressor(n_estimators=100,n_jobs=-1)
  #reg = DecisionTreeRegressor(max_features='sqrt', splitter='random', min_samples_split=4, max_depth=3)
  reg.fit(X_train, y_train)
  y_pred = reg.predict(X_test)
  y_pred = y_pred.reshape((73230,))
  print(y_pred)
  instance=np.arange(111994,185224)
  csv_write=pd.DataFrame({'Instance':instance,'Income':y_pred})
  csv_write.to_csv("./submission.csv",index=False)

  # print(y_train)
  # print(X_test)
  # print(y_test)
  # print('Score: ', reg.score(X_test, y_test))
  #print("Mean squared error: %f" % mean_squared_error(y_test, y_pred))
  #print('Variance score: %.2f' % r2_score(y_test, y_pred))
  # plt.plot(train_data['Income in EUR'])
  # plt.xlabel('Time (hr)')
  # plt.ylabel('Position (km)')
  # #plt.show()


  # f, axarr = plt.subplots(2, sharex=True)
  # f.set_size_inches(12.5, 7.5)
  # axarr[0].plot(train_data['Year of Record'], train_data['Income in EUR'])
  # axarr[0].plot(x_line, p[0])
  # axarr[0].set_ylabel('CO2 Emissions')
  # axarr[1].plot(train_data['Year of Record'], train_data['Income in EUR'])
  # axarr[1].plot(x_line, p[1])
  # axarr[1].set_xlabel('Year')
  # axarr[1].set_ylabel('Relative temperature')
  # plt.show()

  ## Linear regression model below. Not required tbh
  # train_data_Y = train_data.iloc[:-20,[0,11]]
  # train_data_X = train_data.iloc[:-20,[0,1]]
  # test_data_Y = train_data.iloc[-20:,[0,11]]
  # test_data_X = train_data.iloc[-20:,[0,1]]

  # # test_data_X = test_data.iloc[:,2]
#   regr = linear_model.LinearRegression()

#   # Train the model using the training sets
#   regr.fit(train_data_X, train_data_Y)

#   pred_data_Y = regr.predict(test_data_X)

#   # The coefficients
#   print('Coefficients: \n', regr.coef_)
#   # The mean squared error
#   print("Mean squared error: %.2f"
#         % mean_squared_error(test_data_Y, pred_data_Y))
#   # Explained variance score: 1 is perfect prediction

#   # Plot outputs
#   plt.scatter(diabetes_X_test, diabetes_y_test,  color='black')
#   plt.plot(diabetes_X_test, diabetes_y_pred, color='blue', linewidth=3)

#   plt.xticks(())
#   plt.yticks(())

#   plt.show()
if __name__ == '__main__':
  main()
