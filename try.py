import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from sklearn import datasets, linear_model
from sklearn.metrics import mean_squared_error, r2_score
from sklearn.model_selection import train_test_split
from sklearn.decomposition import PCA
from sklearn.ensemble import RandomForestClassifier,RandomForestRegressor
from sklearn.tree import DecisionTreeRegressor
from sklearn.datasets import make_classification
from sklearn.linear_model import LinearRegression
from mpl_toolkits.mplot3d import Axes3D
from sklearn.preprocessing import LabelEncoder, OneHotEncoder
from sklearn import preprocessing
from sklearn.pipeline import FeatureUnion, Pipeline
from sklearn.base import BaseEstimator, TransformerMixin
from sklearn.preprocessing import Imputer, StandardScaler
from sklearn import utils

def changetype(df):
  dfList = df.tolist()
  returnList = [ float(x) for x in dfList ]
  return returnList

def createPipeline(features):
    """ Creates a pipeline for data cleansing and preparation. """

    pipeline = Pipeline([
        ("selector", DataFrameSelector(features)),
        ("imputer", Imputer(strategy="median")),
        ("std_scaler", StandardScaler())
    ])

    finalPipeline = FeatureUnion(transformer_list=[
        ("pipeline", pipeline)
    ])

    return finalPipeline

class DataFrameSelector(BaseEstimator, TransformerMixin):
    """ Selects a dataframe based on attributeNames. """

    def __init__(self, attributeNames):
        """ Constructor.
        Get attribute names and create an instance variable. """
        self.attributeNames = attributeNames

    def fit(self, X, y=None):
        """ Fit data. """
        return self

    def transform(self, X):
        """ Transform X based on fitted model. """
        return X[self.attributeNames].values

def removeOutliners(dataframe):
    """ Removes all the instances which has an outlier variable. """
    quantile1 = dataframe.quantile(0.25)
    quantile3 = dataframe.quantile(0.75)
    iqr = quantile3 - quantile1
    fenceLow = quantile1 - 1.5 * iqr
    fenceHigh = quantile3 + 1.5 * iqr
    fenceOut = (dataframe > fenceLow) & (dataframe < fenceHigh)
    notOutliers = fenceOut[fenceOut.columns[0]]
    for i, col in enumerate(list(fenceOut.columns)):
        if (i == 0): continue
        notOutliers = notOutliers & fenceOut[col]
    return dataframe.loc[notOutliers]

def main():

  # train_data_list=list()
  # with open('./dataset/training.csv', 'r') as f:
  #   reader = csv.reader(f)
  #   next(reader)
  #   for row in reader:
  #     train_data_list.append(row)
  #   train_data= np.asarray(train_data_list)

  # print ("Train_data_list shape: ", train_data_list[1,1])
  # print ("Train_data shape: ", train_data.shape)
  # X=train_data_list[:, [10,11]]
  # y=train_data_list[:, [0]]
  # print ("X_shape",X.shape)
  #x_min, x_max = X[:, 0].min() - .5, X[:, 0].max() + .5
  #y_min, y_max = X[:, 1].min() - .5, X[:, 1].max() + .5
  # Above code is related to setting up the data as numpy and lists

  train_df=pd.read_csv('./dataset/training.csv')
  test_df=pd.read_csv('./dataset/test.csv')
  # test_data=pd.read_csv('./dataset/test.csv')
  train_data=pd.DataFrame(train_df,columns=['Year of Record','Age','Income in EUR','Gender','Country','Profession','University Degree', 'Size of City', 'Wears Glasses', 'Hair Color', 'Body Height [cm]'])
  train_data=train_data.fillna(train_data.mode().iloc[0])
  #print(train_data.dtypes)
  test_data=pd.DataFrame(test_df,columns=['Year of Record','Age','Gender','Country','Profession','University Degree', 'Size of City', 'Wears Glasses', 'Hair Color', 'Body Height [cm]'])
  test_data=test_data.fillna(test_data.mode().iloc[0])


   # Pre-processing of data
  oe = LabelEncoder()
  le = OneHotEncoder()
  le.fit(train_data['Gender'].astype(str))
  train_data['Gender']=le.transform(train_data['Gender'].astype(str))
  le.fit(train_data['Country'].astype(str))
  train_data['Country']=le.transform(train_data['Country'].astype(str))
  le.fit(train_data['Profession'].astype(str))
  train_data['Profession']=le.transform(train_data['Profession'].astype(str))
  le.fit(train_data['University Degree'].astype(str))
  train_data['University Degree']=le.transform(train_data['University Degree'].astype(str))
  le.fit(train_data['Size of City'].astype(str))
  train_data['Size of City']=le.transform(train_data['Size of City'].astype(str))
  le.fit(train_data['Wears Glasses'].astype(str))
  train_data['Wears Glasses']=le.transform(train_data['Wears Glasses'].astype(str))
  le.fit(train_data['Hair Color'].astype(str))
  train_data['Hair Color']=le.transform(train_data['Hair Color'].astype(str))
  #le.fit(train_data['Income in EUR'].astype(str))
  #train_data['Income in EUR']=le.fit_transform(train_data['Income in EUR'].astype(str))

  #le.fit(train_data['Body Height [cm]'].astype(str))
  #train_data['Body Height [cm]']=le.transform(train_data['Body Height [cm]'].astype(str))

  ## Removes Outliners
  #train_data= removeOutliners(pd.DataFrame(train_data))

  le = LabelEncoder()
  le.fit(test_data['Gender'].astype(str))
  test_data['Gender']=le.transform(test_data['Gender'].astype(str))
  le.fit(test_data['Country'].astype(str))
  test_data['Country']=le.transform(test_data['Country'].astype(str))
  le.fit(test_data['Profession'].astype(str))
  test_data['Profession']=le.transform(test_data['Profession'].astype(str))
  le.fit(test_data['University Degree'].astype(str))
  test_data['University Degree']=le.transform(test_data['University Degree'].astype(str))
  le.fit(test_data['Size of City'].astype(str))
  test_data['Size of City']=le.transform(test_data['Size of City'].astype(str))
  le.fit(test_data['Wears Glasses'].astype(str))
  test_data['Wears Glasses']=le.transform(test_data['Wears Glasses'].astype(str))
  le.fit(test_data['Hair Color'].astype(str))
  test_data['Hair Color']=le.transform(test_data['Hair Color'].astype(str))


  Y = train_data.as_matrix(['Income in EUR'])

  X = train_data.as_matrix(['Year of Record', 'Age','Gender','Country','Profession','University Degree', 'Size of City', 'Wears Glasses', 'Hair Color', 'Body Height [cm]'])
  #X = preprocessing.scale(X)
  #Y = preprocessing.normalize(Y,'l1',0)

  # If test_data is not avalible uncomment below line and comment the rest of them below
  # features_raw = Y
  # skewed = [test_data['Gender'], test_data['University Degree'], test_data['Hair Color']]
  # numerical = [test_data['Year of Record'], test_data['Age'], test_data['Body Height [cm]'], test_data['Size of City']]
  # features_log_transformed = pd.DataFrame(data=features_raw)
  # features_log_transformed[skewed] = features_raw[skewed].apply(lambda x: np.log(x + 1))
  # scaler = MinMaxScaler()
  # features_log_minmax_transform = pd.DataFrame(data = features_log_transformed)
  # features_log_minmax_transform[numerical] = scaler.fit_transform(features_log_transformed[numerical])
  # features_final = pd.get_dummies(features_log_minmax_transform)
  # Y = features_raw
  #scaler = preprocessing.StandardScaler()
#  scaler = preprocessing.MinMaxScaler()
  #pipeline=createPipeline(pd.DataFrame(X).columns.values)
  #X = pipeline.fit_transform(pd.DataFrame(X))
  #X = scaler.fit_transform(X)
  #pc = PCA(10)
  #X = pc.fit_transform(X)
#  Y = scaler.fit_transform(Y)
  #Y_norm = list()
  #for x in Y:
    # 1777867 for 30k
    # 178569.22113 for 40k and 25k
  #  if x > 20000:
  #    Y_norm.append(1)
  #  else: Y_norm.append(0)
 # Y = Y_norm
 #X_train, X_test, y_train, y_test = np.asarray(train_test_split(X, Y, test_size=0.1))
  X_train=X
  y_train=Y
  X_test=test_data.as_matrix(['Year of Record', 'Age','Gender','Country','Profession','University Degree', 'Size of City', 'Wears Glasses', 'Hair Color', 'Body Height [cm]'])

  #X_train = scaler.fit_transform(X_train)
  #X_test = scaler.transform(X_test)
  ## Decreased accuracy by 50%
  #X_train = preprocessing.normalize(X_train, norm='l2')
  #X_test = preprocessing.normalize(X_test, norm='l2')


  #reg = LinearRegression(copy_X=True, fit_intercept=True, n_jobs=1, normalize=False)
  #reg.fit(X_train, y_train)
  #for alpha in [0.0001, 0.001, 0.01, 0.1, 1, 3, 10]:
  #  reg = linear_model.Lasso(alpha = alpha)
  #  reg.fit(X_train, y_train)
  #for alpha in [0.0001, 0.001, 0.01, 0.1, 1, 3, 10]:
     #reg = linear_model.Ridge(alpha = alpha)
  print(utils.multiclass.type_of_target(X_train))
  print(utils.multiclass.type_of_target(y_train))
  y_train = np.ravel(y_train)
  #reg = RandomForestRegressor(n_estimators=50, random_state=10, n_jobs=4,max_depth=4)
  reg = RandomForestRegressor(max_features=10, min_samples_split=4000, n_estimators=50, min_samples_leaf=100)
  #reg = DecisionTreeRegressor(max_features='sqrt', splitter='random', min_samples_split=4, max_depth=3)
  reg.fit(X_train, y_train)
  y_pred = reg.predict(X_test)
  y_pred = y_pred.reshape((73230,))
  print(y_pred)
  instance=np.arange(111994,185224)
  csv_write=pd.DataFrame({'Instance':instance,'Income':y_pred})
  csv_write.to_csv("./submission.csv",index=False)

  # print(y_train)
  # print(X_test)
  # print(y_test)
  # print('Score: ', reg.score(X_test, y_test))
  #print("Mean squared error: %f" % mean_squared_error(y_test, y_pred))
  #print('Variance score: %.2f' % r2_score(y_test, y_pred))
  # plt.plot(train_data['Income in EUR'])
  # plt.xlabel('Time (hr)')
  # plt.ylabel('Position (km)')
  # #plt.show()


  # f, axarr = plt.subplots(2, sharex=True)
  # f.set_size_inches(12.5, 7.5)
  # axarr[0].plot(train_data['Year of Record'], train_data['Income in EUR'])
  # axarr[0].plot(x_line, p[0])
  # axarr[0].set_ylabel('CO2 Emissions')
  # axarr[1].plot(train_data['Year of Record'], train_data['Income in EUR'])
  # axarr[1].plot(x_line, p[1])
  # axarr[1].set_xlabel('Year')
  # axarr[1].set_ylabel('Relative temperature')
  # plt.show()

  ## Linear regression model below. Not required tbh
  # train_data_Y = train_data.iloc[:-20,[0,11]]
  # train_data_X = train_data.iloc[:-20,[0,1]]
  # test_data_Y = train_data.iloc[-20:,[0,11]]
  # test_data_X = train_data.iloc[-20:,[0,1]]

  # # test_data_X = test_data.iloc[:,2]
#   regr = linear_model.LinearRegression()

#   # Train the model using the training sets
#   regr.fit(train_data_X, train_data_Y)

#   pred_data_Y = regr.predict(test_data_X)

#   # The coefficients
#   print('Coefficients: \n', regr.coef_)
#   # The mean squared error
#   print("Mean squared error: %.2f"
#         % mean_squared_error(test_data_Y, pred_data_Y))
#   # Explained variance score: 1 is perfect prediction

#   # Plot outputs
#   plt.scatter(diabetes_X_test, diabetes_y_test,  color='black')
#   plt.plot(diabetes_X_test, diabetes_y_pred, color='blue', linewidth=3)

#   plt.xticks(())
#   plt.yticks(())

#   plt.show()
if __name__ == '__main__':
  main()
